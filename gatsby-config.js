module.exports = {
  siteMetadata: {
    title: 'Raw Talent',
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-fastclick',
    {
      resolve: 'gatsby-plugin-manifest',
      options: {
        name: 'Raw Talent',
        short_name: 'Raw Talent',
        start_url: '/',
        background_color: '#f7f0eb',
        theme_color: '#A3418B',
        display: 'standalone',
        icons: [
          {
            src: `/favicons/favicon.png`,
            sizes: `32x32`,
            type: `image/png`,
          },
        ],
      },
    },
    'gatsby-plugin-offline',
    'gatsby-plugin-styled-components',
    'gatsby-plugin-react-next',
  ],
}
