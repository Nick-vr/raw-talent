import React from 'react'
import Helmet from 'react-helmet'
import styled from 'styled-components'

import PageHeading from '../components/PageHeading'
import DoeMeeForm from '../components/doe-mee/DoeMeeForm'

const Wrapper = styled.section`
  background-color: #323233;
`

export default () => (
  <Wrapper>
    <Helmet>
      <title>Raw Talent - Join Us!</title>
    </Helmet>
    <PageHeading text="Join Us!" />
    <DoeMeeForm />
  </Wrapper>
)
