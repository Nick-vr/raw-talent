import React from 'react'
import Helmet from 'react-helmet'
import Link from 'gatsby-link'
import styled from 'styled-components'

import PageHeading from '../components/PageHeading'

const Wrapper = styled.section`
  background-color: #323233;
  text-align: center;

  p {
    color: #fff;
    font-size: 20px;
    padding: 15px;
  }

  h1 {
    font-size: 60px;
    padding: 100px 0;
  }

  h2 {
    font-size: 40px;
    padding: 100px 0;
    color: #4d4d4d;
  }
`

export default () => (
  <Wrapper>
    <Helmet>
      <title>Raw Talent - Dankuwel</title>
    </Helmet>
    <PageHeading text="Verzonden!" />
    <p>
      Dank u wel om ons te contacteren.<br /> Tot binnenkort!
    </p>
    <Link to="/">
      <h2>TERUG</h2>
    </Link>
  </Wrapper>
)
