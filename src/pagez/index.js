import React from 'react'
import styled from 'styled-components'

import PageHeading from '../components/PageHeading'
import PersonContact from '../components/PersonContact'
import MissionStatement from '../components/index/MissionStatement'
import CoreValues from '../components/index/CoreValues'
import IntroText from '../components/index/IntroText'
import JoinUs from '../components/JoinUs'

import Domien from '../static/images/people/domien.jpg'
import Liezl from '../static/images/people/liezl.jpg'

const doeMeeTekstTop =
  'Ben je benieuwd? Stuur ons dan nu je cv of bel rechtstreeks naar Liezl op 0484 53 51 73.'
const doeMeeTekst =
  'Als jij hebt wat wij zoeken, nodigen we je snel uit voor een bezoek aan de Loft - onze co-working hub – om je te laten verrassen door de meest getalenteerde start up van België!'

const Wrapper = styled.section`
  background-color: #323233;
`

const PersonWrapper = styled.div`
  @media (min-width: 768px) {
    display: flex;
    justify-content: space-around;
    padding: 250px 0 250px;
  }
`

export default () => (
  <Wrapper>
    <PageHeading text="Over ons" />
    <IntroText />
    <PersonWrapper>
      <PersonContact
        image={Domien}
        fullName="Domien Hendrix"
        companyRole="Zaakvoerder / Talent Scout"
        email="domien@rawtalent.be"
      />
      <PersonContact
        image={Liezl}
        fullName="Liezl Dehertefelt"
        companyRole="Office Manager / Talent Scout"
        email="liezl@rawtalent.be"
      />
    </PersonWrapper>

    <MissionStatement />
    <CoreValues />
    <JoinUs textTop={doeMeeTekstTop} text={doeMeeTekst} />
  </Wrapper>
)
