import React from 'react'
import Helmet from 'react-helmet'
import styled from 'styled-components'

import PageHeadingWhite from '../components/PageHeadingWhite'
import WatWeDoenArticles from '../components/wat-we-doen/WatWeDoenArticles'
import ContacteerOnsButton from '../components/wat-we-doen/ContacteerOnsButton'
import JoinUs from '../components/JoinUs'

const doeMeeTekstTop =
  'Wil jij ons hierbij helpen? Waag je kans en stuur meteen jouw kandidatuur door. Als wij in jou ons volgende talent zien, contacteren we je snel!'

const Wrapper = styled.section`
  background-color: #a3418b;

  @media (min-width: 1000px) {
    background-color: #323233;
  }
`

export default () => (
  <Wrapper>
    <Helmet>
      <title>Raw Talent - Wat We Doen</title>
    </Helmet>
    <PageHeadingWhite text="Wat we doen" />
    <WatWeDoenArticles />
    <ContacteerOnsButton />
    <JoinUs textTop={doeMeeTekstTop} />
  </Wrapper>
)
