import React from 'react'
import Helmet from 'react-helmet'
import styled from 'styled-components'

import PageHeading from '../components/PageHeading'
import DeviderHeader from '../components/DeviderHeader'
import JoinUs from '../components/JoinUs'
import PeopleCards from '../components/carrieres/PeopleCards'

import wegnaar from '../static/images/wegnaar.svg'

const Wrapper = styled.section`
  background-color: #323233;
`

const CarrieresIntro = styled.div`
  font-size: 16px;
  color: #ffffff;
  padding-bottom: 40px;
  text-align: center;
  p {
    padding: 15px;
  }

  @media (min-width: 768px) {
    font-size: 25px;
    padding: 0 0 100px 0;
  }
`

const CarrieresVanTot = CarrieresIntro.extend`
  text-align: justify;
  p {
    padding-top: 30px;
    font-size: 14px;
  }

  @media (min-width: 768px) {
    padding: 50px 60px;

    p {
      font-size: 18px;
    }
  }

  @media (min-width: 1200px) {
    padding: 100px 250px;

    p {
      font-size: 20px;
    }
  }
`

const WegNaarImg = styled.img`
  display: block;
  margin: 0 auto;
  width: 375px;
  padding: 90px 0 90px;

  @media (max-width: 320px) {
    width: 320px;
  }
`

const textTop =
  'Wil jij deel uitmaken van deze reis?  Stuur jouw cv dan meteen naar ons door.'
const text =
  'Als jouw kwaliteiten overeenkomen met onze vereisten, dan zal je snel horen van ons.'

export default () => (
  <Wrapper>
    <Helmet>
      <title>Raw Talent - Carrières</title>
    </Helmet>
    <PageHeading text="Carrières" />
    <CarrieresIntro>
      <p>
        Mensen zijn het kloppende hart van ons bedrijf en die staan dan ook
        steeds centraal.
      </p>
      <p>
        Wij geloven niet in senioriteit, maar gaan voor een volledige
        horizontale structuur. We leren van elkaar, altijd en overal, ongeacht
        de ervaring.
      </p>
    </CarrieresIntro>
    <DeviderHeader header="Van Brand Ambassador tot Franchisee" />
    <CarrieresVanTot>
      <p>
        Om onze ondernemers sterk te maken, gebruiken wij internationaal erkende
        coachingsystemen in een positieve en dynamische omgeving. Dit maakt een
        traject mogelijk waarin elke BA kan groeien op eigen tempo en naar het
        vooropgestelde doel.
      </p>
      <p>
        Onze Brand Ambassadors zijn het uithangbord van de klanten die wij
        vertegenwoordigen. Op die manier vergroten we niet alleen de
        naamsbekendheid van hun merk, maar hebben we ook een gigantische impact
        op de return on investment door meer omzet te genereren.
      </p>
      <p>
        Onze specialiteit ligt in het trainen en coachen van de BA’s. Hierdoor
        creeen we voor iedereen de mogelijkheid om zichzelf te ontwikkelen en
        door te groeien binnen ons Franchise model. Aan de hand van
        vooropgestelde vaste targets en competenties is het voor eenieder
        mogelijk om op een objectieve manier door te groeien, ongeacht de
        prestaties of senioriteit van anderen.
      </p>
    </CarrieresVanTot>
    <DeviderHeader header="De weg naar Franchisee" />
    <WegNaarImg src={wegnaar} />
    <JoinUs textTop={textTop} text={text} />
    <PeopleCards />
  </Wrapper>
)
