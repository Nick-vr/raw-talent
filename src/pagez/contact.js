import React from 'react'
import Helmet from 'react-helmet'
import Link from 'gatsby-link'
import styled from 'styled-components'

import PageHeading from '../components/PageHeading'
import PersonContact from '../components/PersonContact'
import DeviderHeader from '../components/DeviderHeader'

import Domien from '../static/images/people/domien.jpg'
import Liezl from '../static/images/people/liezl.jpg'

import AddressMap from '../static/images/map.jpg'

const Wrapper = styled.section`
  background-color: #323233;
`

const Address = styled.p`
  font-family: 'Bree Serif', serif;
  text-align: center;
  color: #bababa;
  padding-bottom: 40px;
`

const StyledMap = styled.div`
  width: 100%;
  height: 200px;
  background-image: url(${AddressMap});
  background-position: center;
  margin: 0 auto;
  opacity: 0.3;
`

const PersonWrapper = styled.div`
  padding: 25px 0 150px;
  @media (min-width: 768px) {
    display: flex;
    justify-content: space-around;
    padding: 25px 0 200px;
  }
`

export default () => (
  <Wrapper>
    <Helmet>
      <title>Raw Talent - Contact</title>
    </Helmet>
    <PageHeading text="Contact" />
    <PersonWrapper>
      <PersonContact
        image={Domien}
        fullName="Domien Hendrix"
        companyRole="Zaakvoerder / Talent Scout"
        email="domien@rawtalent.be"
      />
      <PersonContact
        image={Liezl}
        fullName="Liezl Dehertefelt"
        companyRole="Office Manager / Talent Scout"
        email="liezl@rawtalent.be"
      />
    </PersonWrapper>
    <DeviderHeader header="Kom Langs" />
    <PageHeading text="Raw Talent" />
    <Address>
      The Loft – Co-working hub<br />
      Mechelsesteenweg 128<br />
      2018 Antwerpen
    </Address>
    <Link
      to="//goo.gl/maps/tXy55bmHp8q"
      target="_blank"
      rel="noopener noreferrer"
    >
      <StyledMap />
    </Link>
  </Wrapper>
)
