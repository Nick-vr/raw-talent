/* eslint no-unused-expressions: 0 */
import React, { Fragment } from 'react'
import Helmet from 'react-helmet'
import { injectGlobal } from 'styled-components'

import Header from '../components/Header'
import Footer from '../components/Footer'
import MobileBurger from '../components/MobileBurger'

import favicon from '../static/favicons/favicon.png'

injectGlobal`
  * {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
  }

  body {
    font-family: 'Imprima', sans-serif;
    /* font-family: 'Bree Serif', serif; */
  }

  a {
    text-decoration: none;
    outline: 0;
  }

  a:visited {
    text-decoration: none;
    color: #FFFFFF;
  }

  a:link {
    color: #FFFFFF;
  }
`

export default ({ children }) => (
  <Fragment>
    <Helmet>
      <meta charSet="utf-8" />
      <meta
        name="viewport"
        content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"
      />
      <link rel="shortcut icon" href={favicon} />
    </Helmet>
    <div>{children()}</div>
  </Fragment>
)
