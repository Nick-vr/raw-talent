import React from 'react'
import Link from 'gatsby-link'
import styled from 'styled-components'

const Wrapper = styled.div`
  background-color: #fff;
  padding: 50px 0 40px;

  a {
    background-color: #fff;
    display: block;
    text-decoration: none;
    width: 250px;
    margin: 0 auto;
  }
`
const Button = styled.button`
  font-family: 'Bree Serif', serif;
  background-color: #fff;
  border: 4px solid #a3418b;
  color: #a3418b;
  width: 250px;
  height: 100px;
  font-size: 50px;

  transition: 0.5s ease;

  &:hover {
    background-color: #a3418b;
    color: #fff;
    cursor: pointer;
  }
`

export default () => (
  <Wrapper>
    <Link to="/join-us">
      <Button>Join Us</Button>
    </Link>
  </Wrapper>
)
