import React, { Component, Fragment } from 'react'
import styled from 'styled-components'

const FormWrapper = styled.section`
  display: flex;
  margin: 0 auto;
  padding-bottom: 50px;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`

const FormInput = styled.input`
  font-family: 'Bree Serif', serif;
  border: none;
  height: 40px;
  width: 280px;
  margin: 20px 0;
  padding: 5px;
  font-size: 18px;
  &:required {
    box-shadow: none;
  }
`

const FormTextArea = styled.textarea`
  font-family: 'Bree Serif', serif;
  border: none;
  min-height: 100px;
  max-width: 280px;
  min-width: 280px;
  margin: 20px 0;
  padding: 5px;
  font-size: 18px;
  &:required {
    box-shadow: none;
  }
`

const FormButton = styled.button`
  font-family: 'Bree Serif', serif;
  background-color: #a3418b;
  color: #323233;
  border: none;
  padding: 10px;
  width: 280px;
  transition: 0.5s ease;

  &:hover {
    cursor: pointer;
    color: #a3418b;
    background-color: #3b3b3b;
  }
`

const encode = data =>
  Object.keys(data)
    .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key]))
    .join('&')

class DoeMeeForm extends Component {
  state = {
    naam: '',
    telefoonnummer: '',
    email: '',
    talenten: '',
    opmerkingen: '',
  }

  handleSubmit = e => {
    fetch('/', {
      method: 'POST',
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      body: encode({ 'form-name': 'doe-mee', ...this.state }),
    })
      .then(() => (window.location.href = '/dankuwel'))
      .catch(error => console.log(error))

    e.preventDefault()
  }

  handleChange = e => this.setState({ [e.target.name]: e.target.value })

  render() {
    const { naam, telefoonnummer, email, talenten, opmerkingen } = this.state
    return (
      <Fragment>
        <form
          name="doe-mee"
          method="post"
          action="/dankuwel/"
          data-netlify="true"
          data-netlify-honeypot="bot-field"
          onSubmit={this.handleSubmit}
        >
          <FormWrapper>
            <FormInput
              type="text"
              name="naam"
              placeholder="Naam"
              value={naam}
              innerRef={input => (this.naamInput = input)}
              onMouseEnter={() => this.naamInput.focus()}
              onChange={this.handleChange}
              required
            />
            <FormInput
              type="tel"
              name="telefoonnummer"
              placeholder="Telefoonnummer"
              value={telefoonnummer}
              innerRef={input => (this.telefoonInput = input)}
              onMouseEnter={() => this.telefoonInput.focus()}
              onChange={this.handleChange}
              required
            />
            <FormInput
              type="email"
              name="email"
              placeholder="Email"
              value={email}
              innerRef={input => (this.emailInput = input)}
              onMouseEnter={() => this.emailInput.focus()}
              onChange={this.handleChange}
              required
            />
            <FormTextArea
              type="text"
              name="talenten"
              placeholder="Wat zijn jouw talenten?"
              value={talenten}
              innerRef={input => (this.talentenInput = input)}
              onMouseEnter={() => this.talentenInput.focus()}
              onChange={this.handleChange}
              required
            />
            <FormTextArea
              type="text"
              name="opmerkingen"
              placeholder="Opmerkingen"
              value={opmerkingen}
              innerRef={input => (this.opmerkingenInput = input)}
              onMouseEnter={() => this.opmerkingenInput.focus()}
              onChange={this.handleChange}
              required
            />
            <FormInput type="hidden" name="form-name" value="doe-mee" />
            <FormButton type="submit">VERZENDEN</FormButton>
          </FormWrapper>
        </form>
      </Fragment>
    )
  }
}

export default DoeMeeForm
