import React from 'react'
import Link from 'gatsby-link'
import styled from 'styled-components'

import facebook from '../static/images/facebook.svg'
import instagram from '../static/images/instagram.svg'

const Wrapper = styled.div`
  display: flex;
  height: 50px;
`

const SocialIcon = styled.img`
  width: 30px;
  margin: 5px;
`

const SocialIconLink = styled.a`
  margin: 5px;
`

const BaSvg = styled.svg`
  background-color: #a3418b;
  border-radius: 50%;
  width: 30px;
  height: 30px;
  padding: 5px;
  margin-top: 5px;
  margin-left: 5px;
  fill: #323233;
`

const SocialButtons = () => (
  <Wrapper>
    <SocialIconLink
      href="//www.facebook.com/rawtalentmarketing"
      target="_blank"
      rel="noopener noreferrer"
    >
      <SocialIcon src={facebook} alt="Facebook" />
    </SocialIconLink>
    <SocialIconLink
      href="//instagram.com/rawtalentmarketing"
      target="_blank"
      rel="noopener noreferrer"
    >
      <SocialIcon src={instagram} alt="Instagram" />
    </SocialIconLink>
    <SocialIconLink href="//brandambassador.life" target="_blank" rel="noopener noreferrer">
      <BaSvg
        version="1.1"
        x="0px"
        y="0px"
        viewBox="0 0 60 43"
        enableBackground="new 0 0 60 43"
        xmlSpace="preserve"
      >
        <title>Brand Ambassador</title>
        <path d="M0,0.114h5.275v15.261c0.049-0.029,0.097-0.06,0.143-0.093c2.49-2.2,5.607-3.565,8.913-3.903
    c4.013-0.521,8.079,0.428,11.446,2.672c2.211,1.487,3.895,3.637,4.809,6.142c1.019,2.679,1.938,5.384,2.717,8.138
    c0.498,1.879,1.271,3.676,2.291,5.332c1.388,2.248,3.771,3.688,6.406,3.873c1.9,0.266,3.838,0.055,5.638-0.613
    c3.84-1.32,6.544-4.771,6.907-8.814c0.566-5.611-3.406-10.666-8.993-11.44c-3.907-0.714-7.891,0.804-10.332,3.938
    c-0.07,0.082-0.14,0.169-0.229,0.277c-1.087-1.468-2.158-2.914-3.269-4.414c0.563-0.51,1.097-1.043,1.681-1.521
    c2.428-2.022,5.408-3.271,8.554-3.577c4.845-0.591,9.696,1.046,13.192,4.451c2.426,2.268,4.045,5.268,4.61,8.539
    c0.225,1.16,0.292,2.346,0.201,3.523c-0.019,0.211-0.021,0.424-0.021,0.637c-0.001,4.527-0.001,9.057-0.001,13.584v0.369h-5.272
    v-3.689c-0.074,0.043-0.146,0.092-0.215,0.143c-2.47,2.166-5.56,3.504-8.829,3.82c-5.45,0.717-10.888-1.404-14.41-5.625
    c-1.033-1.244-1.811-2.684-2.286-4.229c-0.704-2.117-1.38-4.246-2.043-6.377c-0.646-2.326-1.743-4.502-3.231-6.403
    c-1.379-1.791-3.369-3.013-5.592-3.433c-2.994-0.574-6.091,0.17-8.496,2.045c-4.601,3.393-5.58,9.871-2.188,14.471
    c0.307,0.416,0.645,0.811,1.011,1.176c1.739,1.813,4.071,2.941,6.572,3.18c3.196,0.391,6.399-0.668,8.733-2.887
    c0.215-0.195,0.414-0.406,0.609-0.619c0.188-0.205,0.365-0.418,0.563-0.646c1.082,1.459,2.148,2.9,3.255,4.395
    c-0.57,0.506-1.11,1.035-1.701,1.504c-2.733,2.195-6.108,3.439-9.614,3.547C9.023,43.209,2.094,37.961,0.337,30.373
    C0.21,29.787,0.145,29.189,0.049,28.6C0.036,28.549,0.02,28.498,0,28.447V0.114L0,0.114z" />
      </BaSvg>
    </SocialIconLink>
  </Wrapper>
)

export default SocialButtons
