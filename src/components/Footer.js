import React from 'react'
import styled from 'styled-components'
import Link from 'gatsby-link'

// Components
import SocialButtons from './SocialButtons'

// Images
import logo from '../static/images/logo-notalent.png'

const Wrapper = styled.footer`
  display: flex;
  justify-content: center;
  flex-direction: column;
  background-color: #3b3b3b;
`

const Logo = styled.img`
  display: block;
  margin: 30px auto;
  width: 160px;
  height: auto;
  opacity: 0.8;
`

const FooterLink = styled.p`
  font-size: 28px;
  align-self: center;
  padding: 20px;
  transition: linear 0.5s;
  opacity: 0.3;
  &:hover {
    opacity: 0.4;
  }
`

const AddressName = styled.p`
  font-size: 16px;
  text-align: center;
  color: #8c8c8c;
  margin: 30px 0 30px 0;
`

const FooterSocial = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
  background-color: #4d4d4d;
  filter: grayscale(100%);
`

const Footer = () => (
  <Wrapper>
    <Logo src={logo} alt="logo" />
    <FooterLink>
      <Link to="/join-us">JOIN US</Link>
    </FooterLink>
    <FooterLink>
      <Link to="/contact">CONTACT</Link>
    </FooterLink>
    <AddressName>
      The Loft – Co-working hub<br />
      Mechelsesteenweg 128<br />
      2018 Antwerpen
    </AddressName>
    <FooterSocial>
      <SocialButtons />
    </FooterSocial>
  </Wrapper>
)

export default Footer
