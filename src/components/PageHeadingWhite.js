import React from 'react'
import styled from 'styled-components'

const Header = styled.h1`
  text-align: center;
  font-family: 'Bree Serif', serif;
  font-size: 40px;
  padding: 60px 0 60px;
  color: ${props => (props.white ? '#FFF' : '#a3418b')};
  font-weight: 100;
`

const PageHeadingWhite = props => <Header white>{props.text}</Header>

export default PageHeadingWhite
