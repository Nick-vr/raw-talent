import React, { Fragment } from 'react'

import WatWeDoenArticle from './WatWeDoenArticle'

const watWeDoen = [
  {
    id: 0,
    title: 'Risico vrij werven van klanten',
    body:
      'Elk bedrijf is op zoek naar nieuwe klanten, maar weet vaak niet de beste manier om dit aan te pakken. Wij gaan daarom op zoek naar klanten voor bedrijven.',
  },
  {
    id: 1,
    title: 'Return on investment',
    body:
      'Elke dag spreken onze Brand Ambassadors met consumenten over de merken die wij vertegenwoordigen. Op deze manier voorzien wij de merken van een schitterende customer service.',
  },
  {
    id: 2,
    title: 'Brand building',
    body:
      'Door de vele interacties met potentiële klanten helpen wij merken met het vergroten van hun naamsbekendheid.',
  },
]

const WatWeDoenArticles = () => (
  <Fragment>
    {watWeDoen.map(item => (
      <WatWeDoenArticle key={item.id} title={item.title} body={item.body} />
    ))}
  </Fragment>
)

export default WatWeDoenArticles
