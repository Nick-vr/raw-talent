import React, { Fragment } from 'react'
import styled from 'styled-components'

const StyledH1 = styled.h1`
  font-family: 'Bree Serif', serif;
  font-weight: 100;
  color: #474747;
  background-color: #ffffff;
  margin: 0 10px 20px 10px;
  font-size: 20px;
  text-align: center;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.05), -4px 4px 4px rgba(0, 0, 0, 0.05),
    4px 4px 4px rgba(0, 0, 0, 0.05);

  @media (min-width: 420px) {
    margin: 0 50px 20px 50px;
  }

  @media (min-width: 768px) {
    margin: 0 150px 20px 150px;
  }

  @media (min-width: 1000px) {
    margin: 0 250px 20px 250px;
    font-size: 26px;
  }

  @media (min-width: 1400px) {
    margin: 50px 450px 20px 450px;
  }
`

const Body = styled.p`
  color: #ffffff;
  margin: 0 10px 0 10px;
  padding-bottom: 60px;
  text-align: justify;

  @media (min-width: 420px) {
    margin: 0 50px 0px 50px;
  }

  @media (min-width: 768px) {
    margin: 0 150px 0px 150px;
  }

  @media (min-width: 1000px) {
    margin: 0 250px 0px 250px;
    font-size: 22px;
  }

  @media (min-width: 1400px) {
    margin: 0 450px 50px 450px;
  }
`

const WatWeDoenArticle = props => (
  <Fragment>
    <StyledH1>{props.title}</StyledH1>
    <Body>{props.body}</Body>
  </Fragment>
)

export default WatWeDoenArticle
