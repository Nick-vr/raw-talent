import React from 'react'
import Link from 'gatsby-link'
import styled from 'styled-components'

const Wrapper = styled.div`
  background-color: #323233;
  padding: 50px 0 40px;

  a {
    display: block;
    text-decoration: none;
    width: 250px;
    margin: 0 auto;
  }
`
const Button = styled.button`
  font-family: 'Bree Serif', serif;
  border: 2px solid #ffffff;
  color: #ffffff;
  background-color: #323233;
  width: 250px;
  height: 80px;
  font-size: 25px;

  transition: 0.5s ease;

  &:hover {
    background-color: #ffffff;
    color: #323233;
    cursor: pointer;
  }
`

const ContactButton = () => (
  <Wrapper>
    <Link to="/join-us">g
      <Button>Contacteer ons</Button>
    </Link>
  </Wrapper>
)

export default ContactButton
