import React from 'react'
import styled from 'styled-components'
import Link from 'gatsby-link'

// Components
import SocialButtons from './SocialButtons'

// Images
import logo from '../static/images/logo-notalent.png'
import skyline from '../static/images/skyline.jpg'

const Head = styled.header`
  display: flex;
  flex-direction: column;
  background-color: #323233;
`

const HeaderTop = styled.div`
  display: flex;
  justify-content: space-between;
  border-bottom: 3px solid #a3418b;
`

const HeaderLogo = styled.img`
  height: 30px;
  margin: 10px 0 0 10px;
`

const Quote = styled.section`
  background-color: #fff;
  text-align: center;
  padding: 10px 0 10px 0;

  @media (min-width: 768px) {
    display: none;
  }
`
const CenterQuote = styled.div`
  display: inline-block;
  text-align: left;
  line-height: 180%;
  background-color: #fff;
`
const Quotes = styled.span`
  color: #a3418b;
  font-weight: 400;
  font-size: 30px;
`

const QuoteName = styled.span`
  color: #a3418b;
  font-size: 12px;
`

const SkylineImg = styled.img`
  background-image: url(${skyline});
  background-size: cover;
  width: 100%;
  height: 300px;
  filter: grayscale(60%);
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.1), -4px 4px 4px rgba(0, 0, 0, 0.1),
    4px 4px 4px rgba(0, 0, 0, 0.1);

  @media (max-width: 768px) {
    display: none;
  }
`

export default () => (
  <Head>
    <HeaderTop>
      <Link to="/">
        <HeaderLogo src={logo} alt="Logo" />
      </Link>
      <SocialButtons />
    </HeaderTop>
    <SkylineImg />
    <Quote>
      <CenterQuote>
        <Quotes>&ldquo;</Quotes>
        HARD WORK BEATS<br />
        TALENT WHEN TALENT<br />
        DOESN&apos;T WORK HARD
        <Quotes>&rdquo;</Quotes>
        <br />
        <QuoteName>-TIM NOTKE</QuoteName>
      </CenterQuote>
    </Quote>
  </Head>
)
