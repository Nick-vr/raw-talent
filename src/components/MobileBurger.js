/* eslint no-confusing-arrow: 0 */
import React, { Component, Fragment } from 'react'
import Link from 'gatsby-link'
import styled from 'styled-components'

const Container = styled.div`
  z-index: 2;
  display: inline-block;
  position: fixed;
  cursor: pointer;
  right: 30px;
  bottom: 30px;
  background-color: #fff;
  border-radius: 50%;
  width: 45px;
  height: 45px;
  padding: 7px 10px;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.15), -4px 4px 4px rgba(0, 0, 0, 0.15),
    4px 4px 4px rgba(0, 0, 0, 0.15);

  @media (max-width: 768px) {
    right: 15px;
    bottom: 15px;
  }
`

const Bar1 = styled.div`
  width: 25px;
  height: 2px;
  background-color: #333;
  margin: 6px 0;
  transition: 0.4s;
  transform: ${props =>
    props.opened ? 'rotate(-45deg) translate(-6px, 6px)' : ''};
`
const Bar2 = styled.div`
  width: 25px;
  height: 2px;
  background-color: #333;
  margin: 6px 0;
  transition: 0.4s;
  opacity: ${props => (props.opened ? 0 : 1)};
`
const Bar3 = styled.div`
  width: 25px;
  height: 2px;
  background-color: #333;
  margin: 6px 0;
  transition: 0.4s;
  transform: ${props =>
    props.opened ? 'rotate(45deg) translate(-5px, -6px)' : ''};
`

const Navigation = styled.nav`
  z-index: 1;
  display: flex;
  visibility: ${props => (props.opened ? 'visible' : 'hidden')};
  opacity: ${props => (props.opened ? '1' : '0')};
  justify-content: center;
  flex-direction: column;
  text-align: center;
  position: fixed;
  width: 100vw;
  height: 100vh;
  background-color: rgba(163, 65, 139, 0.9);
  transition: all 0.5s ease-out;

  span {
    display: block;
    color: #1e1e1e;
    font-size: 25px;
    padding: 20px 0;
    transition: all 0.5s ease;

    &:hover {
      color: #faebd7;
    }
  }
`

class MobileBurger extends Component {
  state = {
    opened: false,
  }

  handleClick = () => {
    const currentState = this.state.opened
    this.setState({
      opened: !currentState,
    })
  }

  render() {
    return (
      <Fragment>
        <Container opened={this.state.opened} onClick={this.handleClick}>
          <Bar1 opened={this.state.opened} />
          <Bar2 opened={this.state.opened} />
          <Bar3 opened={this.state.opened} />
        </Container>
        <Navigation opened={this.state.opened}>
          <Link to="/" onClick={this.handleClick}>
            <span>STARTPAGINA</span>
          </Link>
          <Link to="/wat-we-doen" onClick={this.handleClick}>
            <span>WAT WE DOEN</span>
          </Link>
          <Link to="/join-us" onClick={this.handleClick}>
            <span>JOIN US</span>
          </Link>
          <Link to="/carrieres" onClick={this.handleClick}>
            <span>CARRIERES</span>
          </Link>
          <Link to="/contact" onClick={this.handleClick}>
            <span>CONTACT</span>
          </Link>
        </Navigation>
      </Fragment>
    )
  }
}

export default MobileBurger
