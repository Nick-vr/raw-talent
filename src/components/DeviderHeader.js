import React from 'react'
import styled from 'styled-components'

const Header = styled.h1`
  font-family: 'Bree Serif', serif;
  background-color: #fff;
  text-align: center;
  color: #474747;
  line-height: 180%;
  font-size: 15px;

  @media (min-width: 768px) {
    font-size: 30px;
  }
`

const DeviderHeader = props => <Header>{props.header}</Header>

export default DeviderHeader
