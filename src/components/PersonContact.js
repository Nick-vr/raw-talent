import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  text-align: center;
  margin: 20px 0;

  img {
    border-radius: 50%;
    width: 200px;
    transition: 1s ease;
    filter: grayscale(100%);

    &:hover {
      filter: grayscale(0%);
    }
  }

  h1,
  h2,
  h3 {
    font-weight: 100;
    padding: 15px;
  }

  h1 {
    color: #a3418b;
    font-size: 24px;
  }

  h2 {
    color: #fff;
    font-size: 16px;
    padding-bottom: 5px;
  }

  h3 {
    color: #fff;
    font-size: 14px;
    padding-top: 0;
  }

  a {
    text-decoration: none;
  }
`

export default props => (
  <Wrapper>
    <a href={`mailto: ${props.email}`}>
      <img src={props.image} alt="Foto" />
    </a>
    <h1>{props.fullName}</h1>
    <h2>{props.companyRole}</h2>
    <a href={`mailto: ${props.email}`}>
      <h3>{props.email}</h3>
    </a>
  </Wrapper>
)
