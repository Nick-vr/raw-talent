import React from 'react'
import styled from 'styled-components'

import JoinUsButton from './JoinUsButton'

const Wrapper = styled.section`
  font-family: 'Bree Serif', serif;
  background-color: #fff;
  color: #fff;
  line-height: 180%;
  font-size: 16px;
  padding: 20px 0;

  p {
    color: #4d4d4d;
    padding: 15px;
    text-align: justify;
    line-height: 180%;
    font-size: 14px;

    @media (min-width: 420px) {
      text-align: center;
    }
    @media (min-width: 768px) {
      font-size: 18px;
    }

    @media (min-width: 1200px) {
      font-size: 28px;
      padding: 30px 300px;
    }
  }
`

export default props => (
  <Wrapper>
    <p>{props.textTop}</p>
    <p>{props.text}</p>
    <JoinUsButton />
  </Wrapper>
)
