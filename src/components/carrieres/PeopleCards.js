import React from 'react'
import styled from 'styled-components'

import PeopleCard from './PeopleCard'

import olivia from '../../static/images/people/olivia-walsh.png'
import balder from '../../static/images/people/balder.jpg'
import wares from '../../static/images/people/wares.jpg'
import kim from '../../static/images/people/kim.jpg'
import charlotte from '../../static/images/people/charlotte.jpg'

const cards = [
  {
    id: 0,
    foto: olivia,
    naam: 'Olivia Walsh',
    leeftijd: '18 jaar',
    studies: 'Studeert Internationaal Ondernemen',
    vorigeJob: 'Hostess',
    huidigePitstop: 'Brand Ambassador',
    waaromGestart:
      'Ik heb gekozen om te starten als BA dankzij het ondernemerschap en de mogelijkheden om naast mijn studies reeds te bouwen aan mijn carriere dankzij de doorgroeimogelijkheden. ',
    trots:
      'Toen ik in een van mijn eerste weken meteen 6 fantastische mensen heb kunnen helpen met de veiligheid van hun thuis.',
  },
  {
    id: 1,
    foto: balder,
    naam: 'Balder Lambert',
    leeftijd: '25 jaar',
    studies: 'BSO Kinderzorg',
    vorigeJob: 'Kinderverzorger',
    huidigePitstop: 'Consultant',
    waaromGestart:
      'Ik heb gekozen om te starten als BA omdat ik mezelf wil ontwikkelen, ontzettend graag met mensen omga en liever voor mezelf werk dan voor anderen.',
    trots:
      'Onlangs werd ik uitgenodigd om naar Mallorca te gaan en daar bij te leren van de beste mensen in onze community.',
  },
  {
    id: 2,
    foto: wares,
    naam: 'Wares Nouri',
    leeftijd: '23 jaar',
    studies: 'Industriele wetenschappen en technologie',
    vorigeJob: 'Horeca',
    huidigePitstop: 'Business Coach',
    waaromGestart:
      'Ik wou heel graag de verantwoordelijkheid nemen om een merk, de naam en reputatie van de klant te vertegenwoordigen en dit met de steun van de beste mensen in deze industrie. Verder krijg ik als BA dagelijks nieuwe uitdagingen waardoor ik mezelf kan ontwikkelen naar een steeds betere versie.',
    trots:
      'Enkele weken geleden had ik alle criteria van een Business Coach gehit en mocht ik mijn officiele promotie ontvangen.',
  },
  {
    id: 3,
    foto: kim,
    naam: 'Kim Van Den Broek',
    leeftijd: '28 jaar',
    studies: 'Mode',
    vorigeJob: 'Barista en kledingmaakster',
    huidigePitstop: 'Assistent Franchisee',
    waaromGestart:
      'Ik ben gestart als BA omdat ik het heerlijk vind om met mensen om te gaan en ze te helpen dankzij de merken die wij vertegenwoordigen. De voldoening die ik hiervan krijg, heb ik nog nooit eerder ervaren.',
    trots:
      'Nadat ik halverwege 2017 mijn promotie van Business Coach naar Assistent Franchisee heb gehaald, kreeg ik de kans om meteen een eigen campagne te runnen. Deze kans heb ik met beide handen gegrepen en ik ben vastberaden om er een succes van te maken.',
  },
  {
    id: 4,
    foto: charlotte,
    naam: 'Charlotte Vertongen',
    leeftijd: '26 jaar',
    studies: 'Secretariaat-Talen',
    vorigeJob: 'Store Manager',
    huidigePitstop: 'Franchisee',
    waaromGestart:
      'Als BA kom ik elke dag in contact met mensen en kan ik daarbij echt een verschil maken ik hun leven. Daarnaast vind ik het geweldig om een impact te hebben op de groei van onze klanten terwijl ik mezelf volop ontwikkel.',
    trots:
      'Ik run mijn eigen succesvolle bedrijf, maar ben daarnaast ook een toegewijde mama. Dankzij de opportuniteit als BA en mijn inzet heb ik niet enkel mijn toekomst, maar ook die van mijn familie positief veranderd.',
  },
]

const Wrapper = styled.section`
  background-color: #323233;

  @media (min-width: 1000px) {
    display: flex;
    flex-wrap: wrap;
    justify-content: space-evenly;
    align-content: center;
  }
`

const PeopleCards = () => (
  <Wrapper>
    {cards.map(card => (
      <PeopleCard
        key={card.id}
        foto={card.foto}
        naam={card.naam}
        leeftijd={card.leeftijd}
        studies={card.studies}
        vorigeJob={card.vorigeJob}
        huidigePitstop={card.huidigePitstop}
        waaromGestart={card.waaromGestart}
        trots={card.trots}
      />
    ))}
  </Wrapper>
)

export default PeopleCards
