import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.section`
  display: block;
  margin: 0 auto;
  padding: 100px 0 100px;
  width: 300px;

  @media (min-width: 1000px) {
    margin: 100px 80px;
  }

  @media (min-width: 1400px) {
    margin: 100px 150px;
  }

  img {
    width: 100%;
    filter: grayscale(100%);
  }

  h1 {
    color: #a3418b;
    font-weight: normal;
    font-size: 21px;
    padding: 15px 15px;
  }

  p {
    color: #757575;
    font-size: 14px;
    padding: 0 15px 25px;
  }
`

const Container = styled.div`
  background-color: #fff;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.15), -4px 4px 4px rgba(0, 0, 0, 0.15),
    4px 4px 4px rgba(0, 0, 0, 0.15);
`

const PeopleCards = props => (
  <Wrapper>
    <Container>
      <img src={props.foto} alt="focvto" />

      <h1>Naam</h1>
      <p>{props.naam}</p>

      <h1>Leeftijd</h1>
      <p>{props.leeftijd}</p>

      <h1>Studies</h1>
      <p>{props.studies}</p>

      <h1>Vorige job</h1>
      <p>{props.vorigeJob}</p>

      <h1>Huidige pitstop</h1>
      <p>{props.huidigePitstop}</p>

      <h1>Waarom ik gestart ben als BA</h1>
      <p>{props.waaromGestart}</p>

      <h1>Deze prestatie maakt mij trots</h1>
      <p>{props.trots}</p>
    </Container>
  </Wrapper>
)

export default PeopleCards
