import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.section`
  background-color: #a3418b;
  text-align: center;
  padding: 20px 0;

  h1 {
    font-family: 'Bree Serif', serif;
    display: inline-block;
    color: #a3418b;
    font-size: 30px;
    font-weight: 100;
    background-color: #fff;
    padding: 0 5px 0 5px;
    width: 280px;

    @media (min-width: 768px) {
      font-size: 45px;
      width: 400px;
      margin-top: 30px;
    }
  }

  p {
    padding: 15px;
    text-align: justify;
    color: #fff;
    line-height: 180%;
    font-size: 16px;

    @media (min-width: 768px) {
      text-align: center;
      font-size: 25px;
      padding: 50px;
    }

    @media (min-width: 1200px) {
      padding: 50px 250px;
    }
  }
`

export default () => (
  <Wrapper>
    <h1>Mission statement</h1>
    <p>
      Onze missie is om getalenteerde mensen met zin voor avontuur te begeleiden
      doorheen hun ontwikkeling tot succesvolle ondernemers. We doen dit samen
      om zo een mentale en commerciële skill set te bouwen die de talenten
      meenemen voor de rest van hun leven. De reis die we samen maken is uniek,
      spannend en #lol.
    </p>
  </Wrapper>
)
