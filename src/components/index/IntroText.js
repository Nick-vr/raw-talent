import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.p`
  background-color: #323233;
  padding: 15px;
  text-align: justify;
  color: #fff;
  line-height: 180%;
  font-size: 16px;

  @media (min-width: 768px) {
    font-family: 'Bree Serif', serif;
    text-align: center;
    font-size: 25px;
    padding: 0 100px;
  }
  @media (min-width: 1400px) {
    padding: 0 300px;
  }
`

export default () => (
  <Wrapper>
    Wij zijn een jonge, dynamische start up met specialisaties in verschillende
    domeinen. Onze expertise loopt van het organiseren van events en het coachen
    en trainen van jonge ondernemers tot workshops over leiderschap.
  </Wrapper>
)
