import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  background-color: #323233;
  padding-bottom: 20px;

  h1 {
    color: #fff;
    padding: 20px 15px 0 15px;
    font-weight: 100;
    text-align: center;

    @media (min-width: 768px) {
      font-size: 25px;
    }
  }

  p {
    color: #fff;
    opacity: 0.5;
    padding: 20px 15px 20px 15px;
    text-align: justify;
    line-height: 180%;
    font-size: 14px;

    @media (min-width: 768px) {
      font-size: 18px;
    }
  }
`

const Card = styled.div`
  display: block;
  margin: 0 auto;
  background-color: #303030;
  margin-bottom: 20px;
  width: 95%;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);

  @media (min-width: 768px) {
    width: 60%;
    margin-bottom: 100px;
  }

  @media (min-width: 1200px) {
    width: 40%;
  }
`

export default props => (
  <Wrapper>
    <Card>
      <h1>{props.title}</h1>
      <p>{props.content}</p>
    </Card>
  </Wrapper>
)
