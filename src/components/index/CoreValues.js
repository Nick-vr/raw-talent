import React from 'react'
import styled from 'styled-components'

import PageHeading from '../PageHeading'
import CoreValue from './CoreValue'

const Wrapper = styled.section`
  background-color: #323233;
`

// TODO Map over CoreValues

const CoreValues = () => (
  <Wrapper>
    <PageHeading text="Core values" />
    <CoreValue
      title="Ondernemen zit in ons bloed"
      content="Onze visie is duidelijk, samen vooruit blijven bewegen als ondernemers in een positievomgeving met veel steun, fun en een gezonde dosis competitie."
    />
    <CoreValue
      title="Diversiteit is onze trots"
      content="Wij geven kansen aan mensen met allerlei verschillende achtergronden. Het gaat niet om wie je bent, maar om wie je wil worden."
    />
    <CoreValue
      title="Comfortzones zijn er om doorbroken te worden"
      content="We doen het als team, en toch iedereen op zijn eigen tempo. Zolang het gebeurt onder begeleiding van experts ter zake kan je voldoende fouten maken om jezelf constant te blijven ontwikkelen."
    />
    <CoreValue
      title="Work hard, play hard!"
      content="Samen doelstellingen behalen en nadien gezamenlijk van de beloning genieten is een van de belangrijkste funderingen van wie we zijn."
    />
    <CoreValue
      title="Mensen zijn ons product"
      content="We investeren heel veel in het ontwikkelen van al onze talenten. Dagelijkse interne coaching sessies, internationale trips om bij te leren van andere ondernemers in ons netwerk en het constant heruitvinden van processen zorgen voor uitdaging en zelfontplooiing."
    />
  </Wrapper>
)

export default CoreValues
